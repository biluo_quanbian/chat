namespace Chat.Contracts;

public class Constant
{
    public const string ChatGPT = "ChatGPT";
    
    public const string Github = "Github";
    
    public const string Success = "200";
    
    public const string GetUserGroup  = "GetUserGroup";
}